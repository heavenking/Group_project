<?php
include_once ("vendor/autoload.php");
use App\id\Mobile\Mobile;

if ($_SERVER['REQUEST_METHOD'] == 'POST'){
    $login = new Mobile();
    $login->setData($_POST)->login();
}else {
    header("location:index.php");
}
