<?php
session_start();
if (!empty($_SESSION['user_info'])) {
?>

		<!DOCTYPE html>
		<html lang="en">
		<head>
			<meta charset="utf-8">
			<meta http-equiv="X-UA-Compatible" content="IE=edge">
			<meta name="viewport" content="width=device-width, initial-scale=1">
			<title>Dashboard</title>

			<!-- Global stylesheets -->
			<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet"
				  type="text/css">
			<link href="../../../assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
			<link href="../../../assets/css/minified/bootstrap.min.css" rel="stylesheet" type="text/css">
			<link href="../../../assets/css/minified/core.min.css" rel="stylesheet" type="text/css">
			<link href="../../../assets/css/minified/components.min.css" rel="stylesheet" type="text/css">
			<link href="../../../assets/css/minified/colors.min.css" rel="stylesheet" type="text/css">
			<!-- /global stylesheets -->

			<!-- Core JS files -->
			<script type="text/javascript" src="../../../assets/js/plugins/loaders/pace.min.js"></script>
			<script type="text/javascript" src="../../../assets/js/core/libraries/jquery.min.js"></script>
			<script type="text/javascript" src="../../../assets/js/core/libraries/bootstrap.min.js"></script>
			<script type="text/javascript" src="../../../assets/js/plugins/loaders/blockui.min.js"></script>
			<!-- /core JS files -->

			<!-- Theme JS files -->
			<script type="text/javascript" src="../../../assets/js/plugins/visualization/d3/d3.min.js"></script>
			<script type="text/javascript" src="../../../assets/js/plugins/visualization/d3/d3_tooltip.js"></script>
			<script type="text/javascript" src="../../../assets/js/plugins/forms/styling/switchery.min.js"></script>
			<script type="text/javascript" src="../../../assets/js/plugins/forms/styling/uniform.min.js"></script>
			<script type="text/javascript"
					src="../../../assets/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
			<script type="text/javascript" src="../../../assets/js/plugins/ui/moment/moment.min.js"></script>
			<script type="text/javascript" src="../../../assets/js/plugins/pickers/daterangepicker.js"></script>

			<script type="text/javascript" src="../../../assets/js/core/app.js"></script>
			<script type="text/javascript" src="../../../assets/js/pages/dashboard.js"></script>
			<!-- /theme JS files -->

		</head>

		<body>

		<!-- Main navbar -->
		<div class="navbar navbar-inverse">
			<div class="navbar-header">
				<a class="navbar-brand" href="index.html"><img src="../../../assets/images/logo_light.png" alt=""></a>
				<ul class="nav navbar-nav visible-xs-block">
					<li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
					<li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
				</ul>
			</div>

			<div class="navbar-collapse collapse" id="navbar-mobile">
				<ul class="nav navbar-nav">
					<li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a>
					</li>
					</ul>

				<ul class="nav navbar-nav navbar-right">

					<li class="dropdown dropdown-user">
						<a class="dropdown-toggle" data-toggle="dropdown">
							<img src="../../../assets/images/placeholder.jpg" alt="">
							<span>
								<?php
									echo $_SESSION['user_info']['user_name'];
								?>
							</span>
							<i class="caret"></i>
						</a>

						<ul class="dropdown-menu dropdown-menu-right">
							<li><a href="#"><i class="icon-user-plus"></i> My profile</a></li>
							<li class="divider"></li>
							<li><a href="../../../logout.php"><i class="icon-switch2"></i> Logout</a></li>
						</ul>
					</li>
				</ul>
			</div>
		</div>
		<!-- /main navbar -->


		<!-- Page container -->
		<div class="page-container">

			<!-- Page content -->
			<div class="page-content">

				<!-- Main sidebar -->
				<div class="sidebar sidebar-main">
					<div class="sidebar-content">

						<!-- User menu -->
						<div class="sidebar-user">
							<div class="category-content">
								<div class="media">
									<a href="#" class="media-left"><img src="../../../assets/images/placeholder.jpg"
																		class="img-circle img-sm" alt=""></a>

									<div class="media-body">
										<span class="media-heading text-semibold">
											<?php
												echo $_SESSION['user_info']['user_name'];
											?>
										</span>

										<div class="text-size-mini text-muted">
											<i class="icon-pin text-size-small"></i> &nbsp;Dhaka,Bangladesh
										</div>
									</div>

									<div class="media-right media-middle"></div>
								</div>
							</div>
						</div>
						<!-- /user menu -->


						<!-- Main navigation -->
						<div class="sidebar-category sidebar-category-visible">
							<div class="category-content no-padding">
								<ul class="navigation navigation-main navigation-accordion">

									<li class="active"><a href="index.html"><i class="icon-home4"></i>
											<span>Dashboard</span></a></li>
								</ul>
							</div>
						</div>
						<!-- /main navigation -->

					</div>
				</div>
				<!-- /main sidebar -->


				<!-- Main content -->
				<div class="content-wrapper">

					<!-- Page header -->
					<div class="page-header">
						<div class="page-header-content">
							<div class="page-title">
								<h4><i class="icon-arrow-left52 position-left"></i> <span
											class="text-semibold">Home</span> - Dashboard</h4>
							</div>
						</div>

						<div class="breadcrumb-line">
							<ul class="breadcrumb">
								<li><a href="index.html"><i class="icon-home2 position-left"></i> Home</a></li>
								<li class="active">Dashboard</li>
							</ul>
						</div>
					</div>
					<!-- /page header -->


					<!-- Content area -->
					<div class="content">

						<!-- Main charts -->
						<div class="row">
							<div class="col-lg-7">

								<!-- Traffic sources -->
								<div class="panel panel-flat">
									<div class="panel-heading">
										<h6 class="panel-title">Traffic sources</h6>

										<div class="heading-elements">
											<form class="heading-form" action="#">
												<div class="form-group">
													<label class="checkbox-inline checkbox-switchery checkbox-right switchery-xs">
														<input type="checkbox" class="switch" checked="checked">
														Live update:
													</label>
												</div>
											</form>
										</div>
									</div>

									<div class="container-fluid">
										<div class="row">
											<div class="col-lg-4">
												<ul class="list-inline text-center">
													<li>
														<a href="#"
														   class="btn border-teal text-teal btn-flat btn-rounded btn-icon btn-xs valign-text-bottom"><i
																	class="icon-plus3"></i></a>
													</li>
													<li class="text-left">
														<div class="text-semibold">New visitors</div>
														<div class="text-muted">2,349 avg</div>
													</li>
												</ul>

												<div class="col-lg-10 col-lg-offset-1">
													<div class="content-group" id="new-visitors"></div>
												</div>
											</div>

											<div class="col-lg-4">
												<ul class="list-inline text-center">
													<li>
														<a href="#"
														   class="btn border-warning-400 text-warning-400 btn-flat btn-rounded btn-icon btn-xs valign-text-bottom"><i
																	class="icon-watch2"></i></a>
													</li>
													<li class="text-left">
														<div class="text-semibold">New sessions</div>
														<div class="text-muted">08:20 avg</div>
													</li>
												</ul>

												<div class="col-lg-10 col-lg-offset-1">
													<div class="content-group" id="new-sessions"></div>
												</div>
											</div>

											<div class="col-lg-4">
												<ul class="list-inline text-center">
													<li>
														<a href="#"
														   class="btn border-indigo-400 text-indigo-400 btn-flat btn-rounded btn-icon btn-xs valign-text-bottom"><i
																	class="icon-people"></i></a>
													</li>
													<li class="text-left">
														<div class="text-semibold">Total online</div>
														<div class="text-muted"><span
																	class="status-mark border-success position-left"></span>
															5,378 avg
														</div>
													</li>
												</ul>

												<div class="col-lg-10 col-lg-offset-1">
													<div class="content-group" id="total-online"></div>
												</div>
											</div>
										</div>
									</div>

									<div class="position-relative" id="traffic-sources"></div>
								</div>
								<!-- /traffic sources -->

							</div>

							<div class="col-lg-5">

								<!-- Sales stats -->
								<div class="panel panel-flat">
									<div class="panel-heading">
										<h6 class="panel-title">Sales statistics</h6>

										<div class="heading-elements">
											<form class="heading-form" action="#">
												<div class="form-group">
													<select class="change-date select-sm" id="select_date">
														<optgroup
																label="<i class='icon-watch pull-right'></i> Time period">
															<option value="val1">June, 29 - July, 5</option>
															<option value="val2">June, 22 - June 28</option>
															<option value="val3" selected="selected">June, 15 - June,
																21
															</option>
															<option value="val4">June, 8 - June, 14</option>
														</optgroup>
													</select>
												</div>
											</form>
										</div>
									</div>

									<div class="container-fluid">
										<div class="row text-center">
											<div class="col-md-4">
												<div class="content-group">
													<h5 class="text-semibold no-margin"><i
																class="icon-calendar5 position-left text-slate"></i>
														5,689</h5>
													<span class="text-muted text-size-small">orders weekly</span>
												</div>
											</div>

											<div class="col-md-4">
												<div class="content-group">
													<h5 class="text-semibold no-margin"><i
																class="icon-calendar52 position-left text-slate"></i>
														32,568</h5>
													<span class="text-muted text-size-small">orders monthly</span>
												</div>
											</div>


											<div class="col-md-4">
												<div class="content-group">
													<h5 class="text-semibold no-margin"><i
																class="icon-cash3 position-left text-slate"></i> $23,464
													</h5>
													<span class="text-muted text-size-small">average revenue</span>
												</div>
											</div>
										</div>
									</div>

									<div class="content-group-sm" id="app_sales"></div>
									<div id="monthly-sales-stats"></div>
								</div>
								<!-- /sales stats -->

							</div>
						</div>
						<!-- /main charts -->

						<!-- Footer -->
						<div class="footer text-muted">
							&copy; 2015. <a href="#">Limitless Web App Kit</a> by <a
									href="http://themeforest.net/user/Kopyov" target="_blank">Eugene Kopyov</a>
						</div>
						<!-- /footer -->

					</div>
					<!-- /content area -->

				</div>
				<!-- /main content -->

				<!-- Footer -->
				<div class="navbar navbar-default navbar-sm navbar-fixed-bottom">
					<ul class="nav navbar-nav no-border visible-xs-block">
						<li><a class="text-center collapsed" data-toggle="collapse" data-target="#navbar-second"><i class="icon-circle-up2"></i></a></li>
					</ul>

					<div class="navbar-collapse collapse" id="navbar-second">
						<div class="navbar-text">
							&copy; 2017. <a href="#">Bitm</a> by <a href="http://themeforest.net/user/Kopyov" target="_blank">Project Group 1</a>
						</div>

						<div class="navbar-right">
							<ul class="nav navbar-nav">
								<li><a href="#">Help center</a></li>
							</ul>
						</div>
					</div>
				</div>
				<!-- /footer -->

		</body>
		</html>

		<?php
	} else{
		$_SESSION['fail']= "You are not authorized!";
		header('location:../../../index.php');
	}

?>