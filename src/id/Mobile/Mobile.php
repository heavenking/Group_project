<?php

namespace App\id\Mobile;
use PDO;
class Mobile
{
    private $id = "";
    private $user_name = "";
    private $password = "";
    private $email = "";
    private $token = "";
    private $pdo = "";
    public function __construct()
    {
        session_start();
        $this->pdo = new PDO('mysql:host=localhost;dbname=db_project_group_1','root','');

    }
    public function setData($data = '')
    {
       if (array_key_exists('user_name',$data)) {
           $this->user_name = $data['user_name'];
       }if (array_key_exists('id',$data)) {
           $this->id = $data['id'];
       }if (array_key_exists('password',$data)) {
           $this->password = $data['password'];
       }if (array_key_exists('email',$data)) {
           $this->email = $data['email'];
       }
        return $this;
    }//End of setData
    public function store($username,$password,$email)
    {
        try {
            $query = "INSERT INTO `users_tbl` (`id`, `user_name`, `password`,`email`) VALUES (:id,:username,:password,:email)";
            $stmnt = $this->pdo->prepare($query);
            $stmnt->execute(
                array(
                    ':id' => null,
                    ':username' => $username,
                    ':password' => $password,
                    ':email'=> $email
                )
            );
            if ($stmnt) {
                $_SESSION['message'] = "Successfully Registered.";
                header('location:registration.php');
            }
        }catch (PDOException $e ) {
            echo 'Error' . $e->getMessage();
        }


    }//End of store
    public function login(){
        try{
            $query = "SELECT * FROM `users_tbl` WHERE `user_name` = '$this->user_name' AND `password` = '$this->password'";
            $stmnt = $this->pdo->prepare($query);
            $stmnt->execute();
            $value = $stmnt->fetch();
            if (empty($value)) {
                $_SESSION['fail'] = "Opps! Invalid email or password";
                header('location:index.php');
            } else {
                $_SESSION['user_info'] = $value;
                header('location:views/id/Mobile/dashboard.php');
            }
        }catch (PDOException $e) {
            echo  'Error' . $e->getMessage();
        }
    }//End of login

    public function checkusername($username){
        try{
            $query = "SELECT user_name FROM `users_tbl` WHERE `user_name` = '$username'";
            $stmnt = $this->pdo->prepare($query);
            $stmnt->execute();
            $value = $stmnt->fetch();
            if ($username == "") {
                echo "<i class=\"icon-cancel-circle2 position-left\"></i> Please Enter Username";
            } elseif($value) {
                echo "<i class=\"icon-cancel-circle2 position-left\"></i> $username is already taken";
            }
            else{
                echo "<i class=\"icon-checkmark position-left\"></i> $username is Available";
            }
            return $value;
        }catch (PDOException $e) {
            echo  'Error' . $e->getMessage();
        }
    }//End of checkusername
    public function validusername(){
        try{
            $username = $this->user_name;
            $password = $this->password;
            $email = $this->email;
            $query = "SELECT * FROM `users_tbl` WHERE `user_name` = '$username'";
            $stmnt = $this->pdo->prepare($query);
            $stmnt->execute();
            $value = $stmnt->fetch();
            if($value)
            {
                $_SESSION['message'] = "Username is already taken";
                header('location:registration.php');
            }
            else{
                $this->store($username,$password,$email);
            }
        }catch (PDOException $e) {
            echo  'Error' . $e->getMessage();
        }
    }//End of login


//    public function index($perpage='',$offset=''){
//        try{
//            $query = "SELECT  SQL_CALC_FOUND_ROWS * FROM `sometable` WHERE `delected_at` = '0000-00-00 00:00:00'ORDER BY id DESC LIMIT $perpage OFFSET $offset";
//            $stmnt = $this->pdo->prepare($query);
//            $stmnt->execute();
//            $value = $stmnt->fetchAll();
//            $subquery = "SELECT FOUND_ROWS()";
//            $totalrow = $this->pdo->query($subquery)->fetch(PDO::FETCH_COLUMN);
//            $value['totalrow'] = $totalrow;
//            return $value;
//        }catch (PDOException $e) {
//            echo  'Error' . $e->getMessage();
//        }
//    }//End of index
//    public function show(){
//        try{
//            $query = "SELECT * FROM `sometable` WHERE unique_id="."'".$this->id."'";
//            $stmnt = $this->pdo->prepare($query);
//            $stmnt->execute();
//            $value = $stmnt->fetch();
//            return $value;
//        }catch (PDOException $e) {
//            echo  'Error' . $e->getMessage();
//        }
//    }//End of show
//    public function update()
//    {
//        try {
//            $query = 'UPDATE sometable SET title = :mobile_brand WHERE id = :id';
//            $stmnt = $this->pdo->prepare($query);
//            $stmnt->execute(
//                array(
//                    ':id' => $this->id,
//                    ':mobile_brand' => $this->title
//                )
//            );
//            if ($stmnt) {
//                $_SESSION['message'] = "Successfully Data Updated";
//                header('location:listbrand.php');
//            }
//        }
//        catch (PDOException $e) {
//            echo  'Error' . $e->getMessage();
//        }
//    }//End of update
//    public function trash()
//    {
//        try {
//            $query = 'UPDATE sometable SET delected_at = :delete_time WHERE id = :id';
//            $stmnt = $this->pdo->prepare($query);
//            $stmnt->execute(
//                array(
//                    ':id' => $this->id,
//                    ':delete_time' => date('Y-m-d h:m:s')
//                )
//            );
//            if ($stmnt) {
//                $_SESSION['message'] = "Successfully Data Deleted";
//                header('location:listbrand.php');
//            }
//        }
//        catch (PDOException $e) {
//            echo  'Error' . $e->getMessage();
//        }
//    }//End of Trash
//    public function trashted()
//    {
//        try{
//            $query = "SELECT * FROM `sometable` WHERE `delected_at` != '0000-00-00 00:00:00'";
//            $stmnt = $this->pdo->prepare($query);
//            $stmnt->execute();
//            $value = $stmnt->fetchAll();
//            return $value;
//        }catch (PDOException $e) {
//            echo  'Error' . $e->getMessage();
//        }
//    }//End of Trashted
//    public function restore()
//    {
//        try {
//            $query = 'UPDATE sometable SET delected_at = :delete_time WHERE id = :id';
//            $stmnt = $this->pdo->prepare($query);
//            $stmnt->execute(
//                array(
//                    ':id' => $this->id,
//                    ':delete_time' => '0000-00-00 00:00:00'
//                )
//            );
//            if ($stmnt) {
//                $_SESSION['message'] = "Successfully Data Restore";
//                header('location:trashlist.php');
//            }
//        }
//        catch (PDOException $e) {
//            echo  'Error' . $e->getMessage();
//        }
//    }//End of Trash
//    public function delete(){
//        try{
//            $pdo = new PDO('mysql:host=localhost;dbname=bitm','root','');
//            $query = "DELETE FROM `sometable` WHERE `sometable`.`id` =".$this->id;
//            $stmnt = $this->pdo->query($query);
//            $stmnt->execute();
//            if ($stmnt) {
//                $_SESSION['message'] = "Successfully Data Deleted";
//                header('location:trashlist.php');
//            }
//        }catch (PDOException $e) {
//            echo  'Error' . $e->getMessage();
//        }
//    }//End of delete


}
